import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SinCompraComponent } from './sin-compra.component';

describe('SinCompraComponent', () => {
  let component: SinCompraComponent;
  let fixture: ComponentFixture<SinCompraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SinCompraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SinCompraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
