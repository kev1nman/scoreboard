import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConCompraComponent } from './con-compra.component';

describe('ConCompraComponent', () => {
  let component: ConCompraComponent;
  let fixture: ComponentFixture<ConCompraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConCompraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConCompraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
