import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-vendedores',
  templateUrl: './vendedores.component.html',
  styleUrls: ['./vendedores.component.scss']
})
export class VendedoresComponent implements OnInit {

  constructor() { }
  optionsSelect: Array<any>;
  ngOnInit() {
    this.optionsSelect = [
      { value: '1', label: 'Terriotorio 1' },
      { value: '2', label: 'Terriotorio 2' },
      { value: '3', label: 'Terriotorio 3' },
    ];
  }

}
