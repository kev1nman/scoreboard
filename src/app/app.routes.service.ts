import { ModalsComponent } from "./views/modals/modals.component";
import { RouterModule, Route } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";
import { NotFoundComponent } from "./views/errors/not-found/not-found.component";
import { Dashboard1Component } from "./views/dashboards/dashboard1/dashboard1.component";

//Maestros
import { VendedoresComponent } from "./views/vendedores/vendedores.component";
import { LoginComponent } from "./views/login/login.component";
import { DistribuidoresComponent } from "./views/distribuidores/distribuidores.component";
import { ConCompraComponent } from "./views/con-compra/con-compra.component";
import { SinCompraComponent } from "./views/sin-compra/sin-compra.component";
import { ResumenGeneralComponent } from "./views/resumen-general/resumen-general.component";
import { SinVentaComponent } from "./views/sin-venta/sin-venta.component";

const routes: Route[] = [
  { path: "", pathMatch: "full", redirectTo: "login" },
  { path: "login", component: LoginComponent },
  { path: "distribuidores", component: DistribuidoresComponent },
  { path: "resumenGeneral", component: ResumenGeneralComponent },
  {
    path: "detalleCliente",
    children: [
      { path: "conCompra", component: ConCompraComponent },
      { path: "sinCompra", component: SinCompraComponent }
    ]
  },
  {
    path: "detalleMaquinas",
    children: [{ path: "sinVenta", component: SinVentaComponent }]
  }
];

export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(routes);
