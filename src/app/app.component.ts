import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Location } from "@angular/common";
import { LoginComponent } from "./views/login/login.component";

@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"]
})
export class AppComponent implements OnInit {
  public navigation: boolean = false;

  values: string[] = ["Tag 1", "Tag 2", "Tag 4"];

  specialPage: boolean;

  private specialPages: any[] = [
    "/pages/login",
    "/pages/register",
    "/pages/lock",
    "/pages/pricing",
    "/pages/single-post",
    "/pages/post-listing"
  ];

  private currentUrl = "";

  constructor(private router: Router, private location: Location) {
    this.router.events.subscribe((route: any) => {
      this.currentUrl = route.url;

      if (this.currentUrl) {
        if (this.currentUrl != "/login") {
          this.navigation = true;
          document
            .getElementById("body-scoreboard")
            .classList.remove("background-login");
          document.getElementById("body-scoreboard").className =
            "mdb-skin fixed-sn";
        } else {
          this.navigation = false;
          document.getElementById("body-scoreboard").className =
            "background-login";
        }
      }

      this.specialPage = this.specialPages.indexOf(this.currentUrl) !== -1;
    });
  }

  ngOnInit(): void {}

  goBack(): void {
    this.location.back();
  }
}
